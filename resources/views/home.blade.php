@extends('layouts.app')

@section('content')
<div class="container-fluid pt-3">
	@isset($summary)
        @php $rowno = $summary->firstItem(); @endphp
        
        @if ($summary->isNotEmpty())
        <div class="bd-example">
            <p class="mb-0">Showing {{ $rowno }} to {{ $summary->lastItem() }} of {{ $summary->total() }} entries <a href="{{ route('download', ['from' => Request::input('from'), 'to' => Request::input('to'), 'type' => Request::input('type')]) }}" class="float-right">Download All</a></p>
        </div>
        @endif
    @endisset

    <table class="table table-hover table-responsive-md">
        <thead>
            <tr>
                <th scope="col" class="border-top-0">&#9608;</th>
                <th scope="col" class="border-top-0">Date</th>
                <th scope="col" class="border-top-0">Patient</th>
                <th scope="col" class="border-top-0">Age</th>
                <th scope="col" class="border-top-0">Doctor</th>
                <th scope="col" class="border-top-0">Payment</th>
            </tr>
        </thead>
        <tbody>
            @isset($summary)
                
                @if ($summary->isEmpty()) <tr> <td colspan="5">No results found</td> </tr> @endif

	            @foreach ($summary as $entry)
	            <tr>
	                <th scope="row">{{ $rowno++ }}</th>
	                <td>{{ $entry->dischdate }}</td>
	                <td>{{ $entry->patientname }}</td>
                    <td>{{ $entry->age }}</td>
	                <td>{{ $entry->doctorname }}</td>
	                <td>{{ $entry->oramount }}</td>
	            </tr>
	            @endforeach
	        @endisset
        </tbody>
    </table>

    @isset($summary)
    <nav aria-label="Page navigation example">
        @php
            $current_item = $summary->currentPage();
            
            $length = $summary->lastPage();
            
            $width = 5; //length of page item
            
            $center = 3; // center of $width
            
            $last_item = $width;

            if ($length > $width) {

                $a = $current_item - $center;

                if ($a > -1) {

                    $last_item = $width + $a;

                    $first_item = $a + 1;

                } else {
                    
                    $first_item = 1;

                }

                if ($last_item > $length) {

                    $last_item = $length;

                    $first_item = ($length - $width) + 1;

                }

            } else {

                $first_item = 1;

                $last_item = $length;

            }
        @endphp

        @if ($summary->isNotEmpty())
        <ul class="pagination justify-content-center">
            <li class="page-item @if (!$summary->previousPageUrl()) disabled @endif">
                <a class="page-link" href="{{ $summary->previousPageUrl() }}" tabindex="-1">Previous</a>
            </li>
            @for ($i = $first_item; $i <= $last_item; $i++)
                <li class="page-item @if ($current_item === $i) active @endif">
                    <a class="page-link" href="{{ route('home', ['from' => Request::input('from'), 'to' => Request::input('to'), 'type' => Request::input('type'), 'page' => $i]) }}">{{ $i }}</a>
                </li>
            @endfor
            <li class="page-item @if (!$summary->nextPageUrl()) disabled @endif">
                <a class="page-link" href="{{ $summary->nextPageUrl() }}">Next</a>
            </li>
        </ul>
        @endif
    </nav>
    @endisset
</div>
@endsection