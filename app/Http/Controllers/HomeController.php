<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        $validator = Validator::make($request->all(), [
            'from' => 'required|date',
            'to'   => 'required|date',
            'type' => 'required',
        ]);

        if (!$validator->fails()) {
            
            $from = collect([$request->input('from'), '00:00:00'])->implode(' ');
            $to   = collect([$request->input('to'), '23:59:59'])->implode(' ');
            $type = $request->input('type');

         /**
          * SELECT   FORMAT(d.dischdate, 'MMMM dd, yyyy') AS dischdate, e.fullname AS patientname, g.age, f.fullname AS doctorname, FORMAT(a.oramount, '#,###,##0.00') as oramount
                  -- c.FK_mscORSeries + ' - ' + CONVERT(nvarchar(50), c.orno) AS orseries, 
                  -- c.orno,
                  -- cast(b.paidamount as numeric(10,2)) as paidamount,
                  -- a.pfamount, 
                  -- a.vatrate, 
                  -- CASE WHEN a.vatrate IS NULL OR
                  --           a.vatrate = 0 THEN 0 
                  --           ELSE (a.pfamount / a.vatrate) * a.vatrate 
                  --           END AS vatamount, 
                  -- a.instrumentfee,
                  -- c.ordate AS ordate,
                  -- a.otheramount,
                  -- a.netamount,
                  -- d.pattrantype, a.fk_pspatregisters
            FROM     psDctrLedgers a, faCRMstrPF b, faCRMstr c, psPatregisters d, psDatacenter e, psDatacenter f, vwreportPatientProfile g
            WHERE    a.PK_TRXNO = b.FK_TRXNO_psDctrLedgers 
                     AND b.FK_TRXNO_faCRMstr = c.PK_TRXNO AND c.cancelflag <> 1
                     AND a.FK_pspatRegisters = d.PK_psPatregisters
                     AND a.FK_emdPatients = e.PK_psDatacenter
                     AND a.FK_emdDoctors = f.PK_psDatacenter
                     AND a.FK_pspatRegisters = g.pk_pspatregisters
                     AND d.dischdate BETWEEN '2018-11-22 00:00:00' AND '2018-11-23 23:59:59' AND d.pattrantype='I'
            GROUP BY d.dischdate, e.fullname, f.fullname, a.oramount, g.age
            ORDER BY e.fullname
          */

            $summary = DB::table('psDctrLedgers as a')
                ->select(DB::raw("FORMAT(d.dischdate, 'MMMM dd, yyyy') AS dischdate"), 'e.fullname AS patientname', 'g.age', 'f.fullname AS doctorname', DB::raw("FORMAT(a.oramount, '#,###,##0.00') as oramount"))
                ->join('faCRMstrPF as b', 'a.PK_TRXNO', '=', 'b.FK_TRXNO_psDctrLedgers')
                ->join('faCRMstr as c', function ($join) {
                    $join->on('b.FK_TRXNO_faCRMstr', '=', 'c.PK_TRXNO')
                         ->where('c.cancelflag', '<>', 1);
                })
                ->join('psPatregisters as d', 'a.FK_pspatRegisters', '=', 'd.PK_psPatregisters')
                ->join('psDatacenter as e', 'a.FK_emdPatients', '=', 'e.PK_psDatacenter')
                ->join('psDatacenter as f', 'a.FK_emdDoctors', '=', 'f.PK_psDatacenter')
                ->join('vwreportPatientProfile as g', 'a.FK_pspatRegisters', '=', 'g.pk_pspatregisters')
                ->whereBetween('d.dischdate', ["$from", "$to"])
                ->where('d.pattrantype', '=', "$type")
                ->groupBy('d.dischdate', 'e.fullname', 'f.fullname', 'a.oramount', 'g.age')
                ->orderBy('e.fullname', 'asc')
                ->paginate(15);

            // Retrieve all of the query string values 
            $query_string = $request->query();

            // Appending To Pagination Links: to append query string to each pagination link
            $summary = $summary->appends($query_string);

            $data['summary'] = $summary;
        }

        return view('home', $data);
    }

    public function download(Request $request)
    {
        $from = collect([$request->input('from'), '00:00:00'])->implode(' ');
        $to   = collect([$request->input('to'), '23:59:59'])->implode(' ');
        $type = $request->input('type');

        $summary = DB::table('psDctrLedgers as a')
            ->select(DB::raw("FORMAT(d.dischdate, 'MMMM dd, yyyy') AS dischdate"), 'e.fullname AS patientname', 'g.age', 'f.fullname AS doctorname', DB::raw('CONVERT(DECIMAL(10,2), a.oramount) as oramount'))
            ->join('faCRMstrPF as b', 'a.PK_TRXNO', '=', 'b.FK_TRXNO_psDctrLedgers')
            ->join('faCRMstr as c', function ($join) {
                $join->on('b.FK_TRXNO_faCRMstr', '=', 'c.PK_TRXNO')
                     ->where('c.cancelflag', '<>', 1);
            })
            ->join('psPatregisters as d', 'a.FK_pspatRegisters', '=', 'd.PK_psPatregisters')
            ->join('psDatacenter as e', 'a.FK_emdPatients', '=', 'e.PK_psDatacenter')
            ->join('psDatacenter as f', 'a.FK_emdDoctors', '=', 'f.PK_psDatacenter')
            ->join('vwreportPatientProfile as g', 'a.FK_pspatRegisters', '=', 'g.pk_pspatregisters')
            ->whereBetween('d.dischdate', ["$from", "$to"])
            ->where('d.pattrantype', '=', "$type")
            ->groupBy('d.dischdate', 'e.fullname', 'f.fullname', 'a.oramount', 'g.age')
            ->orderBy('e.fullname', 'asc')
            ->get();

        // PhpSpreadsheet
        $spreadsheet = new Spreadsheet();

        // Setting a Font name and size
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');

        $spreadsheet->getDefaultStyle()->getFont()->setSize(12);

        $sheet = $spreadsheet->getActiveSheet();
        
        $row = 1;

        $column = 1;

        $headers = ['Date', 'Patient', 'Age', 'Doctor', 'Payment'];

        // Set bold Header
        $sheet->getStyle('1:1')->getFont()->setBold(true);

        // Freezing first line https://www.askingbox.com/question/phpexcel-freeze-first-line-and-column
        $sheet->freezePane('A2');

        for (; $column <= count($headers); $column++) { 
            
            $header = $headers[$column - 1];
            
            // Setting a cell value by column and row
            $sheet->setCellValueByColumnAndRow($column, $row, $header);

            // Setting a column's width
            $sheet->getColumnDimensionByColumn($column)->setAutoSize(true);

        }

        // Config Content
        foreach ($summary as $key => $value) {

            ++$row;

            $pf = collect($value);

            $keys = $pf->keys();

            for ($column = 1; $column <= $pf->count(); $column++) { 
                
                $key = $keys[$column - 1];

                $value = $pf->get($key);

                $datatype = $column > 4 && is_numeric($value) ? DataType::TYPE_NUMERIC : DataType::TYPE_STRING;

                // Retrieving a cell by column and row, and Explicitly set a cell's datatype and value
                $sheet->getCellByColumnAndRow($column, $row)->setValueExplicit($value, $datatype);

                switch ($datatype) {

                    case DataType::TYPE_NUMERIC:

                        // Setting a Number Format
                        $sheet->getCellByColumnAndRow($column, $row)->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                        
                        break;

                    default: break;

                }

            }

        }

        $writer = new Xlsx($spreadsheet);

        $filename = 'Professional Fees Summary.xlsx';

        $writer->save($filename);

        return redirect("/$filename");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
