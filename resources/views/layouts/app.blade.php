<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('library/bootstrap/4.1.3/css/bootstrap.min.css') }}">

        <title>{{ config('app.name') }}</title>

        <link rel="stylesheet" href="{{ asset('library/app.css') }}">        
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark border-bottom">
            <a class="navbar-brand" href="javascript:void(0)">{{ config('app.name') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#filters" aria-controls="filters" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="filters">
                <form class="form-inline my-2 my-lg-0" action="{{ route('home') }}" method="GET">
                    <div class="input-group mr-sm-2 mb-2 mb-sm-0">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">From</span>
                        </div>
                        <input type="date" class="form-control" name="from" value="{{ Request::input('from') }}">
                    </div>
                    <div class="input-group mr-sm-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon3">To</span>
                        </div>
                        <input type="date" class="form-control" name="to" value="{{ Request::input('to') }}">
                    </div>
                    <select class="form-control mr-sm-2 mt-2 mt-sm-0" name="type">
                        <option @if (!in_array(Request::input('type'), ['I','O','E'])) selected @endif disabled hidden>Registry Types</option>
                        <option value="I" @if (Request::input('type') === 'I') selected @endif>Inpatient</option>
                        <option value="O" @if (Request::input('type') === 'O') selected @endif>Outpatient</option>
                        <option value="E" @if (Request::input('type') === 'E') selected @endif>Emergency</option>
                    </select>
                    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Apply Filters</button>
                </form>
            </div>
        </nav>

        @yield('content')

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ asset('library/jquery/3.3.1/jquery.slim.min.js') }}"></script>
        <script src="{{ asset('library/popper/1.14.3/popper.min.js') }}"></script>
        <script src="{{ asset('library/bootstrap/4.1.3/js/bootstrap.min.js') }}"></script>
    </body>
</html>